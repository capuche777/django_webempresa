-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-07-2018 a las 00:23:15
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `webempresa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can add permission', 2, 'add_permission'),
(5, 'Can change permission', 2, 'change_permission'),
(6, 'Can delete permission', 2, 'delete_permission'),
(7, 'Can add group', 3, 'add_group'),
(8, 'Can change group', 3, 'change_group'),
(9, 'Can delete group', 3, 'delete_group'),
(10, 'Can add user', 4, 'add_user'),
(11, 'Can change user', 4, 'change_user'),
(12, 'Can delete user', 4, 'delete_user'),
(13, 'Can add content type', 5, 'add_contenttype'),
(14, 'Can change content type', 5, 'change_contenttype'),
(15, 'Can delete content type', 5, 'delete_contenttype'),
(16, 'Can add session', 6, 'add_session'),
(17, 'Can change session', 6, 'change_session'),
(18, 'Can delete session', 6, 'delete_session'),
(19, 'Can add servicio', 7, 'add_service'),
(20, 'Can change servicio', 7, 'change_service'),
(21, 'Can delete servicio', 7, 'delete_service'),
(22, 'Can add categoría', 8, 'add_category'),
(23, 'Can change categoría', 8, 'change_category'),
(24, 'Can delete categoría', 8, 'delete_category'),
(25, 'Can add entrada', 9, 'add_post'),
(26, 'Can change entrada', 9, 'change_post'),
(27, 'Can delete entrada', 9, 'delete_post'),
(28, 'Can add enlace', 10, 'add_link'),
(29, 'Can change enlace', 10, 'change_link'),
(30, 'Can delete enlace', 10, 'delete_link'),
(31, 'Can add página', 11, 'add_page'),
(32, 'Can change página', 11, 'change_page'),
(33, 'Can delete página', 11, 'delete_page');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$100000$Ae7YRk43AMQv$AhKPk7DbVfxSvxEEOACCOdecxLH7a/CE6M7RC/Sud3I=', '2018-07-05 15:21:20.141133', 1, 'capuche777', '', '', 'capuche777@gmail.com', 1, 1, '2018-06-28 17:15:22.777482');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog_category`
--

CREATE TABLE `blog_category` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `blog_category`
--

INSERT INTO `blog_category` (`id`, `name`, `created`, `updated`) VALUES
(1, 'general', '2018-07-02 20:26:12.287537', '2018-07-02 20:26:12.287537'),
(2, 'Ofertas', '2018-07-02 20:26:20.562555', '2018-07-02 20:26:20.562555');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog_post`
--

CREATE TABLE `blog_post` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` longtext NOT NULL,
  `published` datetime(6) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `author_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `blog_post`
--

INSERT INTO `blog_post` (`id`, `title`, `content`, `published`, `image`, `created`, `updated`, `author_id`) VALUES
(1, 'Ofertas de Otoño', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec leo dui, vehicula vel dapibus ac, tempus non enim. Nunc tempor vel lacus vel gravida. Nam sit amet pellentesque mi. Aliquam eget porta mi, quis fermentum metus.\r\n\r\nCurabitur efficitur pellentesque tellus nec volutpat. In viverra mattis sem, facilisis condimentum mi rutrum ut. Quisque ut pellentesque dui. Nullam eu vehicula metus. Pellentesque id interdum elit.\r\n\r\nAenean in efficitur enim.', '2018-07-02 20:25:29.000000', 'blog/products-01_tlhz4zn.jpg', '2018-07-02 20:26:25.800491', '2018-07-02 20:44:55.664224', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog_post_categories`
--

CREATE TABLE `blog_post_categories` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `blog_post_categories`
--

INSERT INTO `blog_post_categories` (`id`, `post_id`, `category_id`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2018-06-30 04:50:35.530483', '1', 'CAFÉS Y TÉS', 1, '[{\"added\": {}}]', 7, 1),
(2, '2018-06-30 04:51:41.474553', '2', 'Bollería artesanal', 1, '[{\"added\": {}}]', 7, 1),
(3, '2018-06-30 04:51:59.673930', '1', 'Cafés y Tés', 2, '[{\"changed\": {\"fields\": [\"title\", \"subtitle\"]}}]', 7, 1),
(4, '2018-06-30 04:53:02.561900', '3', 'También Vendemos', 1, '[{\"added\": {}}]', 7, 1),
(5, '2018-07-02 20:26:12.328778', '1', 'general', 1, '[{\"added\": {}}]', 8, 1),
(6, '2018-07-02 20:26:20.841299', '2', 'Ofertas', 1, '[{\"added\": {}}]', 8, 1),
(7, '2018-07-02 20:26:25.829568', '1', 'Ofertas de Otoño', 1, '[{\"added\": {}}]', 9, 1),
(8, '2018-07-02 20:44:56.051247', '1', 'Ofertas de Otoño', 2, '[{\"changed\": {\"fields\": [\"content\"]}}]', 9, 1),
(9, '2018-07-05 15:22:27.465765', '1', 'Twitter', 1, '[{\"added\": {}}]', 10, 1),
(10, '2018-07-05 15:23:34.517434', '2', 'Facebook', 1, '[{\"added\": {}}]', 10, 1),
(11, '2018-07-05 15:24:03.039164', '3', 'Instagram', 1, '[{\"added\": {}}]', 10, 1),
(12, '2018-07-05 15:46:56.854357', '2', 'Facebook', 2, '[{\"changed\": {\"fields\": [\"key\"]}}]', 10, 1),
(13, '2018-07-05 15:47:14.857213', '3', 'Instagram', 2, '[{\"changed\": {\"fields\": [\"key\", \"url\"]}}]', 10, 1),
(14, '2018-07-05 15:47:28.857781', '1', 'Twitter', 2, '[{\"changed\": {\"fields\": [\"key\"]}}]', 10, 1),
(15, '2018-07-05 15:47:38.982692', '1', 'Twitter', 2, '[{\"changed\": {\"fields\": [\"key\"]}}]', 10, 1),
(16, '2018-07-05 15:58:11.605696', '3', 'Instagram', 2, '[{\"changed\": {\"fields\": [\"url\"]}}]', 10, 1),
(17, '2018-07-05 16:49:36.047484', '1', 'Política de privacidad', 1, '[{\"added\": {}}]', 11, 1),
(18, '2018-07-05 16:49:45.372588', '2', 'Aviso Legal', 1, '[{\"added\": {}}]', 11, 1),
(19, '2018-07-05 16:50:02.051941', '3', 'Cookies', 1, '[{\"added\": {}}]', 11, 1),
(20, '2018-07-05 20:34:39.947368', '3', 'Cookies', 2, '[{\"changed\": {\"fields\": [\"order\"]}}]', 11, 1),
(21, '2018-07-05 20:34:48.920358', '2', 'Aviso Legal', 2, '[{\"changed\": {\"fields\": [\"order\"]}}]', 11, 1),
(22, '2018-07-05 21:34:26.511426', '1', 'Política de privacidad', 2, '[{\"changed\": {\"fields\": [\"content\"]}}]', 11, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(8, 'blog', 'category'),
(9, 'blog', 'post'),
(5, 'contenttypes', 'contenttype'),
(11, 'pages', 'page'),
(7, 'services', 'service'),
(6, 'sessions', 'session'),
(10, 'social', 'link');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2018-06-28 17:13:27.521392'),
(2, 'auth', '0001_initial', '2018-06-28 17:13:39.207484'),
(3, 'admin', '0001_initial', '2018-06-28 17:13:41.891623'),
(4, 'admin', '0002_logentry_remove_auto_add', '2018-06-28 17:13:41.969831'),
(5, 'contenttypes', '0002_remove_content_type_name', '2018-06-28 17:13:43.483865'),
(6, 'auth', '0002_alter_permission_name_max_length', '2018-06-28 17:13:45.050026'),
(7, 'auth', '0003_alter_user_email_max_length', '2018-06-28 17:13:46.059712'),
(8, 'auth', '0004_alter_user_username_opts', '2018-06-28 17:13:46.134913'),
(9, 'auth', '0005_alter_user_last_login_null', '2018-06-28 17:13:46.920001'),
(10, 'auth', '0006_require_contenttypes_0002', '2018-06-28 17:13:47.015255'),
(11, 'auth', '0007_alter_validators_add_error_messages', '2018-06-28 17:13:47.096471'),
(12, 'auth', '0008_alter_user_username_max_length', '2018-06-28 17:13:48.096129'),
(13, 'auth', '0009_alter_user_last_name_max_length', '2018-06-28 17:13:49.424664'),
(14, 'sessions', '0001_initial', '2018-06-28 17:13:49.994182'),
(15, 'services', '0001_initial', '2018-06-30 03:16:47.519794'),
(16, 'blog', '0001_initial', '2018-07-02 17:44:40.949824'),
(17, 'blog', '0002_auto_20180704_1510', '2018-07-04 21:10:26.378969'),
(18, 'social', '0001_initial', '2018-07-05 15:16:34.788549'),
(19, 'pages', '0001_initial', '2018-07-05 16:46:45.286314'),
(20, 'pages', '0002_auto_20180705_1431', '2018-07-05 20:31:51.537114'),
(21, 'pages', '0003_auto_20180705_1512', '2018-07-05 21:12:25.325046');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('3sa649c63u772r5iocvtp4enu3u77g25', 'MjAzZTdjNDBlNWJmNjEyMzE1ZTZmOWI0YTMxZGI4MzA5ZjFkZWY2MDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkYmRhMTI1NDJhYmE2MzBjMjFmNzgzOTNlYTMwMmM5YmE3MmMzNjMxIn0=', '2018-07-14 04:05:15.493231'),
('4e0b8rimai9sj7yigoduofuyp5r5x8zs', 'MjAzZTdjNDBlNWJmNjEyMzE1ZTZmOWI0YTMxZGI4MzA5ZjFkZWY2MDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkYmRhMTI1NDJhYmE2MzBjMjFmNzgzOTNlYTMwMmM5YmE3MmMzNjMxIn0=', '2018-07-16 20:25:12.523535'),
('9ida9cqmcuqyeiks1n0tdevi01khvzc6', 'MjAzZTdjNDBlNWJmNjEyMzE1ZTZmOWI0YTMxZGI4MzA5ZjFkZWY2MDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkYmRhMTI1NDJhYmE2MzBjMjFmNzgzOTNlYTMwMmM5YmE3MmMzNjMxIn0=', '2018-07-12 17:16:03.925112'),
('9zxusmdhs6rwhe8tllp7l9cypyithxdx', 'MjAzZTdjNDBlNWJmNjEyMzE1ZTZmOWI0YTMxZGI4MzA5ZjFkZWY2MDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkYmRhMTI1NDJhYmE2MzBjMjFmNzgzOTNlYTMwMmM5YmE3MmMzNjMxIn0=', '2018-07-19 15:21:20.191004'),
('gnaurmivsadzjzdyufsk4rvknm5542p1', 'MjAzZTdjNDBlNWJmNjEyMzE1ZTZmOWI0YTMxZGI4MzA5ZjFkZWY2MDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkYmRhMTI1NDJhYmE2MzBjMjFmNzgzOTNlYTMwMmM5YmE3MmMzNjMxIn0=', '2018-07-19 15:05:21.599741'),
('xtuga4cr1cm2mknou8rjk81t6hs9kq4o', 'MjAzZTdjNDBlNWJmNjEyMzE1ZTZmOWI0YTMxZGI4MzA5ZjFkZWY2MDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkYmRhMTI1NDJhYmE2MzBjMjFmNzgzOTNlYTMwMmM5YmE3MmMzNjMxIn0=', '2018-07-16 17:43:46.474901');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pages_page`
--

CREATE TABLE `pages_page` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` longtext NOT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `order` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pages_page`
--

INSERT INTO `pages_page` (`id`, `title`, `content`, `created`, `updated`, `order`) VALUES
(1, 'Política de privacidad', '<p style=\"text-align:justify\"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ipsum nulla, scelerisque sed dui eu, fringilla consequat velit. Integer euismod, eros sit amet facilisis rutrum, elit massa iaculis ex, vitae gravida velit dolor at nunc. Proin tincidunt pellentesque lacus. Cras egestas ligula eu purus finibus, et convallis sem rhoncus. Fusce porta magna nisl. Aenean sed quam at tellus sollicitudin facilisis. </strong></p>\r\n\r\n<p style=\"text-align:justify\"><em>Pellentesque eleifend placerat luctus. Proin semper nisi quam, faucibus pharetra sapien laoreet et. Proin sit amet aliquam mi. Aliquam rhoncus tortor a accumsan condimentum. Maecenas nisl urna, auctor vel aliquam ut, accumsan sit amet ipsum. Nulla at felis lorem. </em></p>\r\n\r\n<p style=\"text-align:justify\"><u>Phasellus sit amet consectetur lacus. Praesent eget feugiat sem. Nam odio neque, luctus quis velit sagittis, aliquet consequat arcu. Duis eget molestie turpis, nec mattis sem. Donec sem risus, rutrum quis elementum ut, pulvinar et lorem. Quisque egestas justo id justo sodales, id commodo lorem condimentum. Mauris hendrerit felis id diam pellentesque, vel lobortis lectus sagittis. Proin efficitur dui justo, sed laoreet mi sollicitudin dapibus. Donec a convallis lorem. Sed laoreet suscipit gravida. Nam tincidunt in justo sit amet rhoncus. Donec et lectus est. Mauris a felis eu risus tincidunt congue ac sit amet orci. Nam placerat varius elit, eu vulputate nibh consequat eget.</u></p>', '2018-07-05 16:49:36.032520', '2018-07-05 21:34:26.492476', 0),
(2, 'Aviso Legal', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ipsum nulla, scelerisque sed dui eu, fringilla consequat velit. Integer euismod, eros sit amet facilisis rutrum, elit massa iaculis ex, vitae gravida velit dolor at nunc. Proin tincidunt pellentesque lacus. Cras egestas ligula eu purus finibus, et convallis sem rhoncus. Fusce porta magna nisl. Aenean sed quam at tellus sollicitudin facilisis. Pellentesque eleifend placerat luctus.\r\n\r\nProin semper nisi quam, faucibus pharetra sapien laoreet et. Proin sit amet aliquam mi. Aliquam rhoncus tortor a accumsan condimentum. Maecenas nisl urna, auctor vel aliquam ut, accumsan sit amet ipsum. Nulla at felis lorem. Phasellus sit amet consectetur lacus. Praesent eget feugiat sem. Nam odio neque, luctus quis velit sagittis, aliquet consequat arcu. Duis eget molestie turpis, nec mattis sem. Donec sem risus, rutrum quis elementum ut, pulvinar et lorem. Quisque egestas justo id justo sodales, id commodo lorem condimentum.\r\n\r\nMauris hendrerit felis id diam pellentesque, vel lobortis lectus sagittis. Proin efficitur dui justo, sed laoreet mi sollicitudin dapibus. Donec a convallis lorem. Sed laoreet suscipit gravida. Nam tincidunt in justo sit amet rhoncus. Donec et lectus est. Mauris a felis eu risus tincidunt congue ac sit amet orci. Nam placerat varius elit, eu vulputate nibh consequat eget.', '2018-07-05 16:49:45.282781', '2018-07-05 20:34:48.817846', 2),
(3, 'Cookies', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ipsum nulla, scelerisque sed dui eu, fringilla consequat velit. Integer euismod, eros sit amet facilisis rutrum, elit massa iaculis ex, vitae gravida velit dolor at nunc. Proin tincidunt pellentesque lacus. Cras egestas ligula eu purus finibus, et convallis sem rhoncus. Fusce porta magna nisl. Aenean sed quam at tellus sollicitudin facilisis. Pellentesque eleifend placerat luctus.\r\n\r\nProin semper nisi quam, faucibus pharetra sapien laoreet et. Proin sit amet aliquam mi. Aliquam rhoncus tortor a accumsan condimentum. Maecenas nisl urna, auctor vel aliquam ut, accumsan sit amet ipsum. Nulla at felis lorem. Phasellus sit amet consectetur lacus. Praesent eget feugiat sem. Nam odio neque, luctus quis velit sagittis, aliquet consequat arcu. Duis eget molestie turpis, nec mattis sem. Donec sem risus, rutrum quis elementum ut, pulvinar et lorem. Quisque egestas justo id justo sodales, id commodo lorem condimentum.\r\n\r\nMauris hendrerit felis id diam pellentesque, vel lobortis lectus sagittis. Proin efficitur dui justo, sed laoreet mi sollicitudin dapibus. Donec a convallis lorem. Sed laoreet suscipit gravida. Nam tincidunt in justo sit amet rhoncus. Donec et lectus est. Mauris a felis eu risus tincidunt congue ac sit amet orci. Nam placerat varius elit, eu vulputate nibh consequat eget.', '2018-07-05 16:50:02.048948', '2018-07-05 20:34:39.936385', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services_service`
--

CREATE TABLE `services_service` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `subtitle` varchar(200) NOT NULL,
  `content` longtext NOT NULL,
  `image` varchar(100) NOT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `services_service`
--

INSERT INTO `services_service` (`id`, `title`, `subtitle`, `content`, `image`, `created`, `updated`) VALUES
(1, 'Cafés y Tés', 'La mezcla perfecta', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In massa purus, posuere non neque sed, ultricies ornare magna. Donec eu lobortis ex. Morbi maximus tempor accumsan. Mauris pellentesque mauris id nibh vestibulum, id sodales lorem bibendum. Nunc at interdum velit. Donec nec sapien eget arcu imperdiet bibendum', 'services/products-01.jpg', '2018-06-30 04:50:35.499486', '2018-06-30 04:51:59.669926'),
(2, 'Bollería artesanal', 'El mejor acompañamiento', 'Morbi dapibus malesuada mollis. Nunc cursus velit eget cursus pharetra. Ut feugiat, dui id congue porttitor, sem erat pharetra dui, in ullamcorper quam nulla vitae quam. Sed blandit libero feugiat diam cursus, nec euismod ante consequat. Curabitur nec tincidunt ipsum, in tempor turpis. Sed ut metus a nisl rhoncus placerat.', 'services/products-02.jpg', '2018-06-30 04:51:41.397555', '2018-06-30 04:51:41.397555'),
(3, 'También Vendemos', 'Producto certificado', 'Integer ac elementum lacus, consectetur cursus tellus. Suspendisse faucibus sapien massa, ac pharetra est aliquet a. In interdum feugiat ultrices. Aliquam eget tempor libero. Aliquam sit amet ante quis lectus fermentum bibendum. Ut molestie nulla in tellus euismod feugiat. Nunc varius erat nulla, eu dictum mi ultrices in.', 'services/products-03.jpg', '2018-06-30 04:53:02.498906', '2018-06-30 04:53:02.498906');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `social_link`
--

CREATE TABLE `social_link` (
  `id` int(11) NOT NULL,
  `key` varchar(100) NOT NULL,
  `name` varchar(200) NOT NULL,
  `url` varchar(200) DEFAULT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `social_link`
--

INSERT INTO `social_link` (`id`, `key`, `name`, `url`, `created`, `updated`) VALUES
(1, 'LINK_TWITTER', 'Twitter', 'http://twitter.com', '2018-07-05 15:22:27.443824', '2018-07-05 15:47:38.910883'),
(2, 'LINK_FACEBOOK', 'Facebook', 'http://facebook.com', '2018-07-05 15:23:34.500510', '2018-07-05 15:46:56.758613'),
(3, 'LINK_INSTAGRAM', 'Instagram', 'http://instagram.com', '2018-07-05 15:24:03.036167', '2018-07-05 15:58:11.524917');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indices de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indices de la tabla `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indices de la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indices de la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indices de la tabla `blog_category`
--
ALTER TABLE `blog_category`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `blog_post`
--
ALTER TABLE `blog_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_post_author_id_dd7a8485_fk_auth_user_id` (`author_id`);

--
-- Indices de la tabla `blog_post_categories`
--
ALTER TABLE `blog_post_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blog_post_categories_post_id_category_id_ed4f1727_uniq` (`post_id`,`category_id`),
  ADD KEY `blog_post_categories_category_id_2211dae5_fk_blog_category_id` (`category_id`);

--
-- Indices de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indices de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indices de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indices de la tabla `pages_page`
--
ALTER TABLE `pages_page`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `services_service`
--
ALTER TABLE `services_service`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `social_link`
--
ALTER TABLE `social_link`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `blog_category`
--
ALTER TABLE `blog_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `blog_post`
--
ALTER TABLE `blog_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `blog_post_categories`
--
ALTER TABLE `blog_post_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `pages_page`
--
ALTER TABLE `pages_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `services_service`
--
ALTER TABLE `services_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `social_link`
--
ALTER TABLE `social_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Filtros para la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Filtros para la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `blog_post`
--
ALTER TABLE `blog_post`
  ADD CONSTRAINT `blog_post_author_id_dd7a8485_fk_auth_user_id` FOREIGN KEY (`author_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `blog_post_categories`
--
ALTER TABLE `blog_post_categories`
  ADD CONSTRAINT `blog_post_categories_category_id_2211dae5_fk_blog_category_id` FOREIGN KEY (`category_id`) REFERENCES `blog_category` (`id`),
  ADD CONSTRAINT `blog_post_categories_post_id_c34e7be1_fk_blog_post_id` FOREIGN KEY (`post_id`) REFERENCES `blog_post` (`id`);

--
-- Filtros para la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
