from django.apps import AppConfig


class ServicesConfig(AppConfig):
    name = 'services'
    verbose_name = 'Gesor de servicios'
